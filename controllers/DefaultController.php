<?php

namespace PixelHumain\PixelHumain\modules\cotools\controllers;

use CommunecterController;
use Yii;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class DefaultController extends CommunecterController {

  public $version = "v0.1.0";
  public $versionDate = "07/01/2018";
  public $keywords = "tools, opensource, collaboration, CO, communecter";
  public $description = "Any tools used for collaboration for CO Elements";
  public $pageTitle = "CO Tools";

  public function beforeAction($action)
	{
    //parent::initPage();
	  return parent::beforeAction($action);
	}

	public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
        return $controller->renderPartial("index");
      else
      {
        $this->layout = "//layouts/empty";
        return $this->render("index");
      }
  }

  public function actionDoc() 
  {
      return file_get_contents('../../modules/'.$this->module->id.'/index.md');
  }

}
